#! /usr/bin/env python
echo "Example service started at ${DATE}" | systemd-cat -p info

virtualenv="/app/prudicz/.virtualenv/bin/activate"
script="/app/prudicz/src/prudicz.py"
config="/app/prudicz/config.ini"

source ${virtualenv}
python ${script} ${config}
