

import sqlite3
import os
from datetime import datetime

class Sqlite3Adapter:
    def __init__(self):
        self.query(
            '''CREATE TABLE IF NOT EXISTS drawn_history(id INTEGER PRIMARY KEY, number INTEGER, timestamp TIMESTAMP, raw TEXT)''')

    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def query(self, query, data=()):
        try:
            self.db = sqlite3.connect(
                File.relative('../sqlite3.db'), detect_types=sqlite3.PARSE_DECLTYPES)
            self.db.row_factory = self.dict_factory
            cursor = self.db.cursor().execute(query, data)
            self.db.commit()
        except Exception as e:
            self.db.rollback()
            raise e

        return cursor

    def post(self, number, raw):
        return self.query('''INSERT INTO drawn_history(number,timestamp,raw) VALUES (?,?,?)''', (number, datetime.now(), raw))

    def get(self):
        return self.query('''SELECT * FROM drawn_history ORDER BY timestamp DESC''')


class File:
    @staticmethod
    def random_line(path):
        with open(path, 'r', errors='ignore') as f:
            line = next(f)
            for num, aline in enumerate(f, 2):
                if random.randrange(num):
                    continue
                line = aline
            return line
    @staticmethod
    def to_string(path):
        with open(path, 'r') as f:
            return f.read()
    @staticmethod
    def relative(path):
        return os.path.join(os.path.dirname(__file__),path)

class Stats:
    BOARD = {
        'all' : [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36],
        'zero' : [0],
        'black' : [2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35],
        'red' : [1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36],
        'even' : [2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36],
        'odd' : [1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35]
    }

    @classmethod
    def get_basic(cls,data):
        stats = {
            'numbers' : {v:0 for v in cls.BOARD['all']},
            'groups' : {'black' : 0, 'red' : 0, 'even' : 0, 'odd': 0, 'line': 0},
            'sequences' : { 'positive': [], 'negative': []}
        }

        for d in data:
            stats['numbers'][d['number']] += 1

            if d['number'] in cls.BOARD['odd']:
                stats['groups']['odd'] +=1
            if d['number'] in cls.BOARD['even']:
                stats['groups']['even'] +=1
            if d['number'] in cls.BOARD['red']:
                stats['groups']['red'] +=1
            if d['number'] in cls.BOARD['black']:
                stats['groups']['black'] +=1



        for group in [cls.BOARD['red'],cls.BOARD['black']]:
            _ = {'line': 0}
            for d in data:
                if (d['number'] in group) or (d['number'] in cls.BOARD['zero']):
                    _['line'] += 1
                else:
                    stats['groups']['line'] = max(stats['groups']['line'],_['line'])
                    _['line'] = 0


        stats['numbers'] = sorted(stats['numbers'], key=lambda i: stats['numbers'][i])
        stats['sequences']['positive'] = stats['numbers'][-18:]
        stats['sequences']['negative'] = stats['numbers'][:18]

        if stats['groups']['black'] > stats['groups']['red']:
            stats['sequences']['positive']  = [v for v in stats['sequences']['positive']  if v in cls.BOARD['black']]
            stats['sequences']['negative'] = [v for v in stats['sequences']['negative'] if v in cls.BOARD['red']]
        else:
            stats['sequences']['positive']  = [v for v in stats['sequences']['positive']  if v in cls.BOARD['red']]
            stats['sequences']['negative'] = [v for v in stats['sequences']['negative'] if v in cls.BOARD['black']]

        if stats['groups']['even'] > stats['groups']['odd']:
            stats['sequences']['positive'] = [v for v in stats['sequences']['positive'] if v in cls.BOARD['even']]
            stats['sequences']['negative'] = [v for v in stats['sequences']['negative'] if v in cls.BOARD['odd']]
        else:
            stats['sequences']['positive'] = [v for v in stats['sequences']['positive'] if v in cls.BOARD['odd']]
            stats['sequences']['negative'] = [v for v in stats['sequences']['negative'] if v in cls.BOARD['even']]


        return stats


