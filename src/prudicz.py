#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
Prudicz is irc3 based Twitch bot which was created with few purposes.

#1 as Hello-World to the python language
#2 as brute-force ruleta winner for #Grolldir channel
#3 as Hello-World to the Unicorn pHAT LED matrix

It contains following modules/managers:

#1 GmManager
#2 LedManager
#3 RuletaManager

Data are stored in sqlite3
"""

import irc3
import time
import re
import random
import regexp
import asyncio
import helpers
import unicornhat as unicorn
from irc3.plugins.cron import cron
from twitch import TwitchClient


"""Extends irc3 bot functionality by decorator @irc3.extend."""


@irc3.extend
def whisper(bot, target, nick, message):
    """Wrap irc3 bot privmsg to send whispers/pm, extend message to accept list."""
    for m in message:
        bot.privmsg(target, f'/w {nick} {m}')


@irc3.extend
def chat(bot, target, nick, message):
    """Wrap irc3 bot privmsg to send chat message, extend message to accept list."""
    for m in message:
        bot.privmsg(target, f'{"@"+nick+":" if nick else ""} {m}')


"""Different asynchronous managers"""


@irc3.plugin
class GmManager:
    """Grollmoney manager."""

    def __init__(self, bot):
        """Initialization."""
        self.bot = bot
        self.gm = 0
        self.sync = 0

    @regexp.process(regexp.GROLLMANY)
    @irc3.event(regexp.GROLLMANY)
    async def event_grollmany(self, tags, mask, event, target, data):
        """Catch grollmoney event and save grollmoney count."""
        self.gm = data['gm']
        self.sync = time.time()

    async def get(self):
        """Get current amount of grollmoney, sends whisper to bot and waits for whisper from bot."""
        self.bot.whisper(self.bot.config.channel, self.bot.config.channel_bot, ['!gm'])
        now = time.time()

        """delay between sent whisper and received whisper"""
        await asyncio.sleep(30)

        if self.sync > now:
            return self.gm
        else:
            raise Exception('T I  M   E    O     U      T       ...')






class RuletaManager:
    def __init__(self, bot):
        self.bot = bot
        self.db = helpers.Sqlite3Adapter()
        self.current = 1
        self.gm_manager = self.bot.get_plugin(GmManager)


        data = self.db.query('select number from drawn_history order by timestamp desc').fetchall()
        stats = helpers.Stats.get_basic(data)


    @regexp.process(regexp.DRAWN_NUMBER)
    @irc3.event(regexp.DRAWN_NUMBER)
    async def draw_number(self, tags, mask, event, target, data):
        self.db.post(data['drawn_number'], data['raw'])
        '''
        if self.bot.config.nick in data['winners']:
            self.current = await self.gm_manager.get()
            self.current = min(int(self.current/(2**5)),100000)
        else:
            self.current = int(self.current * 2)
        '''
        self.current = 1

        await asyncio.sleep(30)

        #data = self.db.query('select number from drawn_history order by timestamp desc').fetchall()

        self.bot.whisper(self.bot.config.channel,self.bot.config.channel_bot, [f'!ruleta {self.current} b'])

@irc3.plugin
class LedManager:

    COLOR = {
        'red': (255, 0, 0),
        'orange': (255, 127, 0),
        'yellow': (255, 255, 0),
        'green': (0, 255, 0),
        'blue': (0, 127, 255),
        'lightblue': (100, 200, 255),
        'magenta': (255, 0, 255),
        'white': (255, 255, 255)
    }


    def __init__(self,bot):
        self.bot = bot
        self.current = 0

        self.unicorn = unicorn
        self.unicorn.set_layout(unicorn.AUTO)
        self.unicorn.rotation(0)
        self.unicorn.brightness(0.5)


    def pos2coord(self,n):
        width,height = self.unicorn.get_shape()

        y = n//width%height
        x = n%width

        return (x,y)

    @regexp.process(regexp.DRAWNING)
    @irc3.event(regexp.DRAWNING)
    async def drawning(self, tags, mask, event, target, data):
        self.unicorn.set_pixel(*self.pos2coord(self.current), self.COLOR['yellow'])
        self.unicorn.show()

    @regexp.process(regexp.DRAWN_NUMBER)
    @irc3.event(regexp.DRAWN_NUMBER)
    async def draw_number(self, tags, mask, event, target, data):
        if self.bot.config.nick in data['winners']:
            self.unicorn.set_pixel(*self.pos2coord(self.current), self.COLOR['green'])
        else:
            self.unicorn.set_pixel(*self.pos2coord(self.current), self.COLOR['red'])
        self.current += 1
        self.unicorn.set_pixel(*self.pos2coord(self.current), self.COLOR['blue'])
        self.unicorn.show()







@irc3.plugin
class PrudiczController:
    def __init__(self, bot):

        self.bot = bot
        self.db = helpers.Sqlite3Adapter()
        self.gm_manager = self.bot.get_plugin(GmManager)
        self.ruleta_manager = self.bot.get_plugin(RuletaManager)
        self.twitch = TwitchClient(client_id='3x44q8z79w34slpojp9k45ip39ivh5')

        #self.led = LedManager()
        #self.bet = BetManager()


    @regexp.process(regexp.DRAWN_NUMBER)
    @irc3.event(regexp.DRAWN_NUMBER)
    async def whisper(self, tags, mask, event, target, data):
        if 'houska1210' in data['winners']:
            self.bot.chat(self.bot.config.channel,'houska1210',['Gratuluji grollLove'])

    @regexp.process(regexp.PRIVMSG_TO_ME)
    @irc3.event(regexp.PRIVMSG_TO_ME)
    async def dddd(self, tags, mask, event, target, data):
        if re.match(r'.*\bgrollLove\b.*', data):
            if mask.nick in ['otrap89','houska1210','jerry82_k','fantomasmag']:
                self.bot.whisper(self.bot.config.channel,self.bot.config.channel_bot,[f'!give {mask.nick} {random.randint(1,10)}'])
            elif mask.nick in ['krtek2000','grolldir', 'andering', 'pavleenka', 'skila18']:
                self.bot.whisper(self.bot.config.channel,self.bot.config.channel_bot,[f'!give {mask.nick} {random.randint(1,10)}'])
            else:
                self.bot.whisper(self.bot.config.channel,self.bot.config.channel_bot,[f'!give {mask.nick} {random.randint(1,10)}'])









if __name__ == '__main__':
    bot = irc3.IrcBot.from_argv(None)
    bot.loop.run_forever()


