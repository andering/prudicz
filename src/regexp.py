#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""Demonstrate high quality docstrings."""

import irc3
import re
import functools


class process(object):

    def __init__(self, regexp):
        self.regexp = regexp

    def __call__(self, f):
        async def wrapped_f(*args1,**args2):
            if self.regexp.process:
                args2 = self.regexp.process(**args2)
            else:
                args2 = args2

            await f(*args1,**args2)
        return wrapped_f



class raw(str):
    name = None
    re = None
    process = None


GROLLMANY = raw('GROLLMANY')
GROLLMANY.name = 'GROLLMANY'
GROLLMANY.re = r'^(@(?P<tags>\S+) )?:(?P<mask>grollbot!grollbot@grollbot\.tmi\.twitch\.tv) (?P<event>(WHISPER)) (?P<target>\S+) :(?P<data>.*máš [0-9,]+ GrollMoney.*)$'
def _process(**args2):
    args2['data'] = re.match(r'(?P<raw>.*?(?P<gm>[0-9,]+).*)',args2['data']).groupdict()
    args2['data']['gm'] = int(re.sub('[^0-9]','', args2['data']['gm']))
    return args2
GROLLMANY.process = _process


DRAWNING = raw('DRAWNING')
DRAWNING.name = 'DRAWNING'
DRAWNING.re = r'^(@(?P<tags>\S+) )?:(?P<mask>grollbot!grollbot@grollbot\.tmi\.twitch\.tv) (?P<event>(PRIVMSG)) (?P<target>\S+) :(?P<data>Sázky pro toto kolo jsou uzavřeny.*)$'
def _process(**args2):
    return args2
DRAWNING.process = _process


DRAWN_NUMBER = raw('DRAWN_NUMBER')
DRAWN_NUMBER.name = 'DRAWN_NUMBER'
DRAWN_NUMBER.re = r'^(@(?P<tags>\S+) )?:(?P<mask>grollbot!grollbot@grollbot\.tmi\.twitch\.tv) (?P<event>(PRIVMSG)) (?P<target>\S+) :(?P<data>Výherní číslo je [0-9].*)$'
def _process(**args2):
    args2['data'] = re.match(r'(?P<raw>.*?(?P<drawn_number>[0-9,]+)(?:. Výherci:(?P<winners>.*))?.*)',args2['data']).groupdict()
    args2['data']['winners'] = [w.split(' - ')[0] for w in args2['data']['winners'].split(',')] if args2['data']['winners'] else []
    return args2
DRAWN_NUMBER.process = _process


PRIVMSG = raw('PRIVMSG')
PRIVMSG.name = 'PRIVMSG'
PRIVMSG.re = r'^(@(?P<tags>\S+) )?:(?P<mask>\S+!\S+@\S+) (?P<event>(PRIVMSG)) (?P<target>\S+) :(?P<data>.*)$'
def _process(**args2):
    return args2
PRIVMSG.process = _process


PRIVMSG_TO_ME = raw('PRIVMSG_TO_ME')
PRIVMSG_TO_ME.name = 'PRIVMSG_TO_ME'
PRIVMSG_TO_ME.re = r'^(@(?P<tags>\S+) )?:(?P<mask>\S+!\S+@\S+) (?P<event>(PRIVMSG)) (?P<target>\S+) :(?P<data>.*{nick}.*)$'
def _process(**args2):
    return args2
PRIVMSG_TO_ME.process = _process


WHISPER = raw('WHISPER')
WHISPER.name = 'WHISPER'
WHISPER.re = r'^(@(?P<tags>\S+) )?:(?P<mask>\S+!\S+@\S+) (?P<event>(WHISPER)) (?P<target>(#\S+|{nick})) :@{nick}[:,\s]\s*(?P<data>\S+.*)$'
def _process(**args2):
    return args2
WHISPER.process = _process
